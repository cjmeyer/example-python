import sys, logging
import ROOT
import xAODRootAccess.GenerateDVIterators

APP_NAME = "test.py"
max_events = 1000

# Set up message logging
logging.basicConfig(level=logging.DEBUG,format='%(name)-25s %(levelname)-7s %(message)s')
msg=logging.getLogger(APP_NAME)

# Set up the ATLAS xAOD environment
if not ROOT.xAOD.Init(APP_NAME).isSuccess():
  msg.error("Failed to call xAOD::Init")
  sys.exit(1)

# Set up a TFile as input
ifile = ROOT.TFile.Open("/d05/cjmey/data/mc16a.PowhegPy8_ttH125.MxAODDetailed.e6503_s3126_r9364_p3665.h024.root")
if not ifile:
  msg.error("Couldn't open input file")
  sys.exit(1)

# Set up TFile as output
ofile = ROOT.TFile.Open("output.root", "RECREATE")
if not ofile:
  msg.error("Couldn't open output file")
  sys.exit(1)

# Create the TTree manager
mgr = ROOT.xAOD.TTreeMgr(ROOT.xAOD.TEvent.kAthenaAccess)
if not mgr.readFrom(ifile).isSuccess():
  msg.error("Couldn't set up reading of input file")
  sys.exit(1)

# Set up histograms
hmyy = ROOT.TH1F("hmyy", "hmyy", 110, 105.0, 160.0)
hptyy = ROOT.TH1F("hptyy", "hptyy", 100, 0, 1000.0)
hptyy_3jets = ROOT.TH1F("hptyy_3jets", "hptyy_3jets", 100, 0, 1000.0)
hptyy_4jets = ROOT.TH1F("hptyy_4jets", "hptyy_4jets", 100, 0, 1000.0)
hetayy = ROOT.TH1F("hetayy", "hetayy", 10, -2.5, 2.5)
hphiyy = ROOT.TH1F("hphiyy", "hphiyy", 12, -3.14159, 3.14159)

n2jets = 0
n4phs = 0
n_not2 = 0

nc_total = 0
nc_2phot = 0
nc_trig = 0
nc_tight = 0
nc_iso = 0
nc_relpt = 0
nc_window = 0

def do_cutflow(passtrig, photons):
  global nc_total
  global nc_2phot
  global nc_trig
  global nc_tight
  global nc_iso
  global nc_relpt
  global nc_window

  nc_total += 1

  if (photons.size() < 2):
    return
  nc_2phot += 1

  if (not passtrig):
    return
  nc_trig += 1

  tight1 = ord(photons[0].auxdataConst("char")("isTight"))
  tight2 = ord(photons[1].auxdataConst("char")("isTight"))
  if (not tight1 or not tight2):
    return
  nc_tight += 1

  iso1 = ord(photons[0].auxdataConst("char")("isIsoFixedCutLoose"))
  iso2 = ord(photons[1].auxdataConst("char")("isIsoFixedCutLoose"))
  if (not iso1 or not iso2):
    return
  nc_iso += 1

  myy = photons[0].p4() + photons[1].p4()
  if (photons[0].pt()/myy.M() < 0.35 or photons[1].pt()/myy.M() < 0.25):
    return
  nc_relpt += 1

  if (myy.M() < 105e3 or 160e3 < myy.M()):
    return
  nc_window += 1


# Loop over input file
entries = mgr.eventTree().GetEntries()
for entry in xrange(entries):
  if (max_events > 0 and entry > max_events):
    msg.info("Exiting after manually specified "+str(max_events)+" events")
    break

  mgr.eventTree().GetEntry(entry)

  if (entry%1000 == 0):
    msg.info("Running over event "+str(entry)+" out of "+str(entries))

  photons = mgr.eventTree().HGamPhotons 
  jets = mgr.eventTree().HGamAntiKt4EMTopoJets

  if (jets.size() >= 2):
    n2jets = n2jets + 1

  if (photons.size() >= 4):
    n4phs = n4phs + 1

  if (photons.size() != 2):
    n_not2 = n_not2 + 1

  do_cutflow(ord(mgr.eventTree().HGamEventInfo.auxdataConst("char")("isPassedTriggerMatch")), photons)

  if (photons.size() < 2):
    msg.info("Found event with less than two photons?")
    continue

  yy = photons[0].p4() + photons[1].p4()

  # print "isTight: \"" + str(ord(photons[0].auxdataConst("char")("isTight"))) + "\""
  # print "isPassed: \"" + str(ord(mgr.eventTree().HGamEventInfo.auxdataConst("char")("isPassed"))) + "\""
  # print "cutFlow: \"" + str(mgr.eventTree().HGamEventInfo.auxdataConst("int")("cutFlow")) + "\""

  hmyy.Fill(yy.M()/1000.0)
  hptyy.Fill(yy.Pt()/1000.0)
  hetayy.Fill(yy.Eta())
  hphiyy.Fill(yy.Phi())

  if (jets.size() < 4):
    hptyy_3jets.Fill(yy.Pt()/1000.0)
  elif (jets.size() >= 4):
    hptyy_4jets.Fill(yy.Pt()/1000.0)

# Save histogram to output
ofile.cd()
hmyy.Write()
hptyy.Write()
hptyy_3jets.Write()
hptyy_4jets.Write()
hphiyy.Write()
hetayy.Write()

msg.info("Found "+str(n2jets)+" events with 2 or more jets")
msg.info("Found "+str(n_not2)+" events without exactly 2 photons")
msg.info("Found "+str(n4phs)+" events with 4 or more photons")


msg.info("cutflow: " + str(nc_total))
msg.info("cutflow: " + str(nc_2phot))
msg.info("cutflow: " + str(nc_trig))
msg.info("cutflow: " + str(nc_tight))
msg.info("cutflow: " + str(nc_iso))
msg.info("cutflow: " + str(nc_relpt))
msg.info("cutflow: " + str(nc_window))

# Delete TTreeMgr manually since python destructs in the wrong order
del mgr
