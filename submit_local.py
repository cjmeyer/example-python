#!/usr/bin/env python

import os
import condor

pwd = os.environ["PWD"]
nfs_area = "/d05/" + os.environ["USER"] + "/condor"

# Configuration common to all jobs
basejob = condor.JobSpec()
basejob.job_inputs = [pwd + "/test.py"]
basejob.job_setup = pwd + "/setup_node.sh"
basejob.output_dir = nfs_area + "/out"

helper = condor.CondorSubmit(cleanOldJobs=True, inputSamples="local")

# Samples to run over
ntup_path = "/d05/cjmey/data/NTUP_ZLLG"
samples = {
  "mc16a" : [
    "eegammaPt10_35"  : "mc16a_13TeV/mc16a.Sherpa_CT10_eegammaPt10_35.DAOD_EGAM3.e3952_s3126_r9364_r9315_p3282.root",
    "eegammaPt35_70"  : "mc16a_13TeV/mc16a.Sherpa_CT10_eegammaPt35_70.DAOD_EGAM3.e3952_s3126_r9364_r9315_p3282.root",
    "eegammaPt70_140" : "mc16a_13TeV/mc16a.Sherpa_CT10_eegammaPt70_140.DAOD_EGAM3.e3952_s3126_r9364_r9315_p3282.root",
    "eegammaPt140"    : "mc16a_13TeV/mc16a.Sherpa_CT10_eegammaPt140.DAOD_EGAM3.e3952_s3126_r9364_r9315_p3282.root",
    "Zee"             : "mc16a.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.DAOD_EGAM3.e3601_s3126_r9364_r9315_p3517.root",
  ],
  "data" : [
    "data15" : "data15_13TeV/target_data15_Zeeg_p3518.root",
    "data16" : "data16_13TeV/target_data16_Zeeg_p3518.root",
  ]
}

for dtype in samples:
  for sample in samples[dtype]:
    job = basejob

    name = dtype + "_" + sample
    job.name = name
    job.job_area = nfs_area + "/log/" + name

    job.input_dir = ntup_path + "/" + samples[dtype][sample]
    job.run_command = "python test.py " + dtype

    helper.submitJob(job)
