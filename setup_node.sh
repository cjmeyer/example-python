# Setup ATLAS local ROOT base if necessary
[ "`compgen -a | grep localSetupROOT`x" == "x" ] \
  && echo "Going to set up ATLAS local ROOT base from cvmfs" \
  && export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \
  && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

export RUCIO_ACCOUNT='cjmeyer'
export X509_USER_PROXY="$PWD/x509_proxy"
export ALRB_XCACHE_PROXY='root://localhost:57129//'

lsetup emi "asetup AnalysisBase,21.2.60"
