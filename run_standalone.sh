#!/bin/bash

# Where am I?
export NODEDIR="$PWD"
echo "Running on: `uname -a`"
echo "Work area : $NODEDIR"
echo "Start time: `date`"
echo

# Make temp directory needed before setup
mkdir -p /tmp/cjmeyer/.alrb 2>&1

# Setup environment
echo "Setting up environment"
source setup.sh 2>&1
echo

echo "Changing back to work area: $NODEDIR"
cd $NODEDIR
echo

echo "Contents of work area:"
ls
echo

# Prepare input file list
echo cp $INPUT input_data.txt
cp $INPUT input_data.txt

echo cat input_data.txt
cat input_data.txt
echo
echo

# Start running
echo $COMMAND
$COMMAND 2>&1
echo

echo cp output.root ${OUTPUT}
cp output.root ${OUTPUT} 2>&1
echo

echo "Contents of work area:"
ls
echo

# Done!
echo "Done with job, cleaning and exiting!"
rm -rf input_data*.txt x509_proxy output.root setup.sh 2>&1
echo

echo "End time: `date`"
