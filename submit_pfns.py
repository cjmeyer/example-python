#!/usr/bin/env python

import os
import condor

pwd = os.environ["PWD"]
nfs_area = "/d05/" + os.environ["USER"] + "/condor"

# Configuration common to all jobs
basejob = condor.JobSpec()
basejob.job_inputs = [pwd + "/test2.py"]
basejob.job_setup = pwd + "/setup_node.sh"
basejob.run_command = "python test2.py"
basejob.output_dir = nfs_area + "/out"
basejob.x509_proxy = os.environ["X509_USER_PROXY"]

helper = condor.CondorSubmit(cleanOldJobs=True, inputSamples="pfns")

# Samples to run over
samples = {
  "zee_pty_35_70" : "mc16_13TeV.364552.Sherpa_222_NNPDF30NNLO_eegammagamma_pty_35_70.deriv.DAOD_HIGG1D1.e6333_s3126_r9364_p3703",
  "zee_pty_70_140": "mc16_13TeV.364552.Sherpa_222_NNPDF30NNLO_eegammagamma_pty_35_70.deriv.DAOD_HIGG1D1.e6333_s3126_r9364_p3703",
  "zee_pty_140"   : "mc16_13TeV.364552.Sherpa_222_NNPDF30NNLO_eegammagamma_pty_35_70.deriv.DAOD_HIGG1D1.e6333_s3126_r9364_p3703",
}

for sample in samples:
  job = basejob
  job.name = sample
  job.job_area = nfs_area + "/log/" + sample
  job.sample_name = samples[sample]

  helper.submitJob(job)
