import os
import shutil
import glob

import xcache
import xrootd
import local

class JobSpec:
  def __init__(self):
    self.name = None
    self.job_area = None
    self.job_inputs = []
    self.job_setup = None
    self.output_dir = None
    self.output_server = None
    self.run_command = None
    self.input_files = []
    self.x509_proxy = None
    self.sample_name = None
    self.xrootd_output_server = None
    self.xrootd_input_server = None
    self.xrootd_input_dir = None

class CondorSubmit:
  def __init__(self, jobFlavour=None, inputSamples="xcache", inputSourceCode="standalone", outputStorage="nfs", cleanOldJobs=False):
    self.jobFlavour = jobFlavour
    self.inputSamples = inputSamples
    self.inputSourceCode = inputSourceCode
    self.outputStorage = outputStorage
    self.cleanOldJobs = cleanOldJobs

    if self.inputSamples == "xcache" or self.inputSamples == "pfns":
      self.xcache = xcache.xcache()

  def submitJobs(self, jobs):
    for job in jobs :
      self.submitJob(job)

  def makeOutputDirs(self, job):
    job_output_dir = job.output_dir
    merge_output_dir = job.output_dir + "/subjob_output"
    if (self.outputStorage == "xrootd"):
      # Make output directory in xrootd (if necessary)
      if (xrootd.existdir(job_output_dir, job.xrootd_output_server) == False) :
        print ".. Making new xrootd directory: " + job_output_dir
        xrootd.mkdir(job_output_dir, job.xrootd_output_server)
      
      # Make stage directory in xrootd (if necessary)
      if (xrootd.existdir(merge_output_dir, job.xrootd_output_server) == False) :
        print ".. Making new xrootd directory: " + merge_output_dir
        xrootd.mkdir(merge_output_dir, job.xrootd_output_server)
    elif (self.outputStorage == "nfs"):
       if (os.path.isdir(job_output_dir) == False):
         print ".. Making new directory: " + job_output_dir
         os.makedirs(job_output_dir)

       if (os.path.isdir(merge_output_dir) == False):
         print ".. Making new directory: " + merge_output_dir
         os.makedirs(merge_output_dir)
    else:
      print "  ERROR: don't understand outputStorage option:", self.outputStorage
    

  def makeJobDir(self, job):
    # Make local log directory (if necessary)
    if (self.cleanOldJobs and os.path.isdir(job.job_area)):
      shutil.rmtree(job.job_area)
    os.makedirs(job.job_area)
    
  def copyJobFiles(self, job):
    if (self.inputSourceCode == "nfs"):
      shutil.copy("run_nfs.sh", job.job_area + "/run.sh")
      shutil.copy("merge_nfs.sh", job.job_area + "/merge.sh")
    elif (self.inputSourceCode == "standalone"):
      shutil.copy("run_standalone.sh", job.job_area + "/run.sh")
      shutil.copy("merge_standalone.sh", job.job_area + "/merge.sh")
    else:
      print "  ERROR: don't understand inputSourceCode option:", self.inputSourceCode

    shutil.copy(job.job_setup, job.job_area + "/setup.sh")
    if (job.x509_proxy != None):
      shutil.copy(job.x509_proxy, job.job_area + "/x509_proxy")

    for i in job.job_inputs:
      shutil.copy(i, job.job_area)

  def fillFileList(self, job):
    if (self.inputSamples == "xcache"):
      job.input_files = self.xcache.getFiles(job.sample_name)
    elif (self.inputSamples == "pfns"):
      job.input_files = self.xcache.getPFNs(job.sample_name)
    elif (self.inputSamples == "xrootd"):
      job.input_files = xrootd.getFilesInDir(job.input_dir, job.xrootd_input_server)
    elif (self.inputSamples == "local"):
      job.input_files = local.getFilesInDir(job.input_dir)
    else:
      print "  ERROR: don't understand inputSamples option:", self.inputSamples

    for i in range(len(job.input_files)) :
      flist = open(job.job_area + "/input_data_subjob" + str(i) + ".txt", "w")
      flist.write(job.input_files[i])
      flist.close()

  def writeDagCard(self, job):
    fdag = open(job.job_area + "/dagman", "w")
    fdag.write("Job " + job.name + " " + job.job_area + "/submit\n")
    # fdag.write("Retry " + subsamp + " 5\n")
    # fdag.write("Vars " + subsamp + " RETRY=\"$(RETRY)\" MAX_RETRIES=\"$(MAX_RETRIES)\"\n")
    fdag.write("Job " + job.name + "_m " + job.job_area + "/submit_m\n")
    fdag.write("\n")
    fdag.write("Parent " + job.name + " Child " + job.name + "_m\n")
    fdag.write("\n")
    fdag.close()

  def writeRunCard(self, j):
    f = open(j.job_area + "/submit", "w")
    f.write("universe = vanilla\n")
    f.write("\n")
    f.write("executable = " + j.job_area + "/run.sh\n")
    f.write("\n")
    f.write("should_transfer_files = YES\n")
    f.write("\n")
    f.write("output = " + j.job_area + "/output_$(Process)\n")
    f.write("error = " + j.job_area + "/error_$(Process)\n")
    f.write("log = " + j.job_area + "/log_$(Process)\n")
    f.write("\n")
    f.write("transfer_input_files = " + j.job_area + "/setup.sh")
    f.write("," + j.job_area + "/input_data_subjob$(Process).txt")
    if (j.x509_proxy != None):
      f.write("," + j.job_area + "/x509_proxy")
    if len(j.job_inputs) > 0:
      for i in j.job_inputs:
        f.write("," + j.job_area + "/" + i.split("/")[-1])
    f.write("\n\n")
    f.write("+JobFlavour = \"tomorrow\"\n")
    f.write("\n")
    f.write("environment = \"INPUT=input_data_subjob$(Process).txt \\\n")
    f.write("               COMMAND='" + j.run_command + "' \\\n")
    if (len(j.input_files) == 1):
      f.write("               OUTPUT=" + j.output_dir + "/" + j.name + ".root\"\n")
    else:
      f.write("               OUTPUT=" + j.output_dir + "/subjob_output/" + j.name + "_subjob$(Process).root\"\n")
    f.write("\n")
    f.write("queue " + str(len(j.input_files)) + "\n")
    f.close()

  def writeMergeCard(self, j):
    f = open(j.job_area + "/submit_m", "w")
    f.write("universe = vanilla\n")
    f.write("\n")
    f.write("executable = " + j.job_area + "/merge.sh\n")
    f.write("\n")
    f.write("should_transfer_files = YES\n")
    f.write("\n")
    f.write("output = " + j.job_area + "/output_m\n")
    f.write("error = " + j.job_area + "/error_m\n")
    f.write("log = " + j.job_area + "/log_m\n")
    f.write("\n")
    f.write("transfer_input_files = " + j.job_area + "/setup.sh")
    if (j.x509_proxy != None):
      f.write("," + j.job_area + "/x509_proxy")
    f.write("\n\n")
    f.write("+JobFlavour = \"workday\"\n")
    f.write("\n")
    for i in range(len(j.input_files)):
      if (i == 0):
        f.write("arguments = \"" + j.output_dir + "/subjob_output/" + j.name + "_subjob" + str(i) + ".root \\\n")
      elif (i == len(j.input_files)-1):
        f.write("             " + j.output_dir + "/subjob_output/" + j.name + "_subjob" + str(i) + ".root\"\n")
      else:
        f.write("             " + j.output_dir + "/subjob_output/" + j.name + "_subjob" + str(i) + ".root \\\n")
    f.write("\n")
    f.write("environment = \"OUTPUT=" + j.output_dir + "/" + j.name + ".root\"\n")
    f.write("\n")
    f.write("queue\n")
    f.close()


  def submitJob(self, job):
    print "Starting to submit", job.name
    
    self.makeOutputDirs(job)
    self.makeJobDir(job)
    self.copyJobFiles(job)
    self.fillFileList(job)

    self.writeRunCard(job)
    
    if len(job.input_files) == 1:
      print ".. condor_submit " + job.job_area + "/submit"
      os.system("condor_submit " + job.job_area + "/submit")
    else :
      self.writeMergeCard(job)
      self.writeDagCard(job)
    
      print ".. condor_submit_dag " + job.job_area + "/dagman"
      os.system("condor_submit_dag " + job.job_area + "/dagman")


