## Synopsis

This is a very simple example of how to run over xAODs using python. It
requires access to a /cvmfs mount point, and can be setup using:
```
source setup_cvmfs.sh
```
After pointing `ifile` at an appropriate input MxAOD, it can be
run using a simple python call:
```
python test.py
```

## Output

By default, it will loop over all events in an input file, take the leading
two photons from `HGamPhotons` and plot their invariant mass in GeV. The
output is stored in the histogram `myy`, saved in `output.root`.
