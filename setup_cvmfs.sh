# Setup ATLAS local ROOT base if necessary
[ "`compgen -a | grep localSetupROOT`x" == "x" ] \
  && echo "Going to set up ATLAS local ROOT base from cvmfs" \
  && export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \
  && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

export RUCIO_ACCOUNT='cjmeyer'

lsetup git rucio "asetup AnalysisBase,21.2.60"

alias vprox='voms-proxy-init -voms atlas -valid 96:0'
